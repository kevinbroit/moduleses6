﻿﻿import {GreeterSingleton} from 'component/GreeterSingleton'

const singleton = Symbol();
const singletonEnforcer = Symbol();

export class MainApp{
    constructor(enforcer) {
        if(enforcer != singletonEnforcer) throw "Cannot construct singleton";
        
        const greeter =  GreeterSingleton.instance;
        greeter.sayHello();
    }
 
    static get instance() {
        if(!this[singleton]) {
            this[singleton] = new MainApp(singletonEnforcer);
        }
        return this[singleton];
    }
}
