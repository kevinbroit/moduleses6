﻿
export class GreeterService {
    constructor() {

    }
    getGreeting(callback) {
        const thatCallback = callback;
        fetch('app/data/some.json')
          .then(  
            function(response) {  
                if (response.status !== 200) {  
                    console.log('Looks like there was a problem. Status Code: ' +  response.status);  
                    return;  
                }
                response.json().then(function(data) {  
                    thatCallback (data.greeting.en);
                });  
            }  
          )  
          .catch(function(err) {  
              console.log('Fetch Error :-S', err);  
          });
    }
}
