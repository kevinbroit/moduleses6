﻿'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var GreeterService = (function () {
    function GreeterService() {
        _classCallCheck(this, GreeterService);
    }

    _createClass(GreeterService, [{
        key: 'getGreeting',
        value: function getGreeting(callback) {
            var thatCallback = callback;
            fetch('https://jsonplaceholder.typicode.com/posts').then(function (response) {
                if (response.status !== 200) {
                    console.log('Looks like there was a problem. Status Code: ' + response.status);
                    return;
                }
                response.json().then(function (data) {
                    thatCallback(data[0].title);
                });
            })['catch'](function (err) {
                console.log('Fetch Error :-S', err);
            });
        }
    }]);

    return GreeterService;
})();

exports.GreeterService = GreeterService;

