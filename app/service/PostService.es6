﻿
export class GreeterService {
    constructor() {

    }
    getGreeting(callback) {
        const thatCallback = callback;
        fetch('https://jsonplaceholder.typicode.com/posts')
          .then(  
            function(response) {  
                if (response.status !== 200) {  
                    console.log('Looks like there was a problem. Status Code: ' +  response.status);  
                    return;  
                }
                response.json().then(function(data) {  
                    thatCallback (data[0].title);
                });  
            }  
          )  
          .catch(function(err) {  
              console.log('Fetch Error :-S', err);  
          });
    }
}
