﻿"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _componentGreeterSingleton = require('component/GreeterSingleton');

var singleton = Symbol();
var singletonEnforcer = Symbol();

var MainApp = (function () {
    function MainApp(enforcer) {
        _classCallCheck(this, MainApp);

        if (enforcer != singletonEnforcer) throw "Cannot construct singleton";

        var greeter = _componentGreeterSingleton.GreeterSingleton.instance;
        greeter.sayHello();
    }

    _createClass(MainApp, null, [{
        key: "instance",
        get: function get() {
            if (!this[singleton]) {
                this[singleton] = new MainApp(singletonEnforcer);
            }
            return this[singleton];
        }
    }]);

    return MainApp;
})();

exports.MainApp = MainApp;

