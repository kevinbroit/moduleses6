﻿'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _serviceGreeterService = require('service/GreeterService');

var _componentMessagePrinter = require('component/MessagePrinter');

//import {PostService} from 'service/PostService'

var _componentMessagePrinterNice = require('component/MessagePrinterNice');

var singletonGreeter = Symbol();
var singletonGreeterEnforcer = Symbol();

var GreeterSingleton = (function () {
	function GreeterSingleton(enforcer) {
		_classCallCheck(this, GreeterSingleton);

		if (enforcer != singletonGreeterEnforcer) throw "Cannot construct singleton";

		this.messagePrinter = new _componentMessagePrinterNice.MessagePrinterNice();
		//this.messagePrinter = new MessagePrinter();
	}

	_createClass(GreeterSingleton, [{
		key: 'sayHello',
		value: function sayHello() {
			var svcGreeting = new _serviceGreeterService.GreeterService();
			svcGreeting.getGreeting(this.messagePrinter.printOut);
		}

		//showFirstPostTitle(){
		//    const svcPost = new PostService();
		//    svcPost.getPosts(this.messagePrinter.printOut);
		//}
	}], [{
		key: 'instance',
		get: function get() {
			if (!this[singletonGreeter]) {
				this[singletonGreeter] = new GreeterSingleton(singletonGreeterEnforcer);
			}
			return this[singletonGreeter];
		}
	}]);

	return GreeterSingleton;
})();

exports.GreeterSingleton = GreeterSingleton;

