﻿import {GreeterService} from 'service/GreeterService'
import {MessagePrinter} from 'component/MessagePrinter'
//import {PostService} from 'service/PostService'
import {MessagePrinterNice} from 'component/MessagePrinterNice'

const singletonGreeter = Symbol();
const singletonGreeterEnforcer = Symbol();

export class GreeterSingleton{

    constructor(enforcer) {
	    if(enforcer != singletonGreeterEnforcer) throw "Cannot construct singleton";
		
        this.messagePrinter = new MessagePrinterNice();
        //this.messagePrinter = new MessagePrinter();
	}

	static get instance() {
	    if(!this[singletonGreeter]) {
	        this[singletonGreeter] = new GreeterSingleton(singletonGreeterEnforcer);
	    }
	    return this[singletonGreeter];
	}

	sayHello(){
	    const svcGreeting = new GreeterService();
	    svcGreeting.getGreeting(this.messagePrinter.printOut);
	}

	//showFirstPostTitle(){
	//    const svcPost = new PostService();
	//    svcPost.getPosts(this.messagePrinter.printOut);
	//}
}
